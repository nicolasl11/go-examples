package main

import (
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

// more info about sockets here
// https://www.youtube.com/watch?v=CIh8qN7LO8M

func main() {
	http.HandleFunc("/", homeHandler)
	http.HandleFunc("/v1/ws", v1Handler)
	http.HandleFunc("/v3/ws", v3Handler)
	http.HandleFunc("/v4/ws", v3Handler)

	http.ListenAndServe(":8080", nil)
}

var upgrader = websocket.Upgrader{}

// just so we have something to see from the server.
func homeHandler(w http.ResponseWriter, req *http.Request) {
	http.ServeFile(w, req, "index.html")
}

func v1Handler(w http.ResponseWriter, req *http.Request) {
	// if an http request comes here, the connection is upgraded to a socket.
	var conn, _ = upgrader.Upgrade(w, req, nil)

	// we need a go routine for every socket.
	go func(conn *websocket.Conn) {
		for {
			// test for any messages that come in.
			var mType, msg, _ = conn.ReadMessage()

			println("the message is = " + string(msg))
			// write the send message back directly.
			conn.WriteMessage(mType, msg)
		}
	}(conn)
}

func v3Handler(w http.ResponseWriter, req *http.Request) {
	// if an http request comes here, the connection is upgraded to a socket.
	var conn, _ = upgrader.Upgrade(w, req, nil)

	// we need a go routine for every socket.
	go func(conn *websocket.Conn) {
		var ch = time.Tick(5 * time.Second)
		for range ch {
			conn.WriteJSON(User{
				Username: "mike",
				Password: "dfjkdfj",
			})
		}
	}(conn)
}

func v4Handler(w http.ResponseWriter, req *http.Request) {
	// if an http request comes here, the connection is upgraded to a socket.
	var conn, _ = upgrader.Upgrade(w, req, nil)

	// check for messages, error message states if the connection is closed
	go func(conn *websocket.Conn) {
		for {
			var _, _, err = conn.ReadMessage()
			if err != nil {
				conn.Close()
			}
		}
	}(conn)

	// send a message through the socket to the client.
	go func(conn *websocket.Conn) {
		var ch = time.Tick(5 * time.Second)
		for range ch {
			conn.WriteJSON(User{
				Username: "mike",
				Password: "dfjkdfj",
			})
		}
	}(conn)
}

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
