package main

func main() {
	c := make(chan int)

	go func() {
		for i := 0; i < 10; i++ {
			c <- i
		}

		close(c)
	}()

	println(<-c)
}

// instanntly after the println the program is terminated whether or not the
// goroutine is finished.
