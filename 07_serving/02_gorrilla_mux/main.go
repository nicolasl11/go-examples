package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/yosssi/ace"
)

func main() {
	var r = mux.NewRouter()

	r.HandleFunc("/", handleRoot).Methods("GET")
	r.HandleFunc("/duck", handleDuck).Methods("GET")

	http.Handle("/", r)

	http.ListenAndServe(":8080", nil)
}

func handleRoot(w http.ResponseWriter, req *http.Request) {
	tmp, err := ace.Load("index", "", nil)

	if err == nil {
		tmp.Execute(w, nil)
	} else {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func handleDuck(w http.ResponseWriter, req *http.Request) {
	tmp, err := ace.Load("hier", "", nil)

	var vars = mux.Vars(req)

	if err == nil {
		tmp.Execute(w, vars["page"])
	} else {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
