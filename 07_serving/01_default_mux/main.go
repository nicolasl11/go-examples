package main

import (
	"net/http"

	"github.com/yosssi/ace"
)

func main() {
	http.HandleFunc("/", handleRoot)

	var hier hierHandler
	http.Handle("/hier", hier)

	http.ListenAndServe(":8080", nil)
}

func handleRoot(w http.ResponseWriter, req *http.Request) {
	tmp, err := ace.Load("index", "", nil)

	if err == nil {
		tmp.Execute(w, nil)
	} else {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

type hierHandler struct{}

func (c hierHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	tmp, err := ace.Load("hier", "", nil)

	if err == nil {
		tmp.Execute(w, nil)
	}
}
