package main

import "testing"

func TestPersonGetName(t *testing.T) {
	var p *Person = &Person{
		Name: "john",
		Age:  12,
	}

	if p.Name != "john" {
		t.Error("expected", "john", "got", p.Name)
	}
}
