package main

import "fmt"

func swap(a *int, b *int) {
	var t int = *a
	*a = *b
	*b = t
}

func main() {
	var a int = 3
	var b int = 16

	fmt.Println("a and b are: ", a, b)

	swap(&a, &b)

	fmt.Println("a and b are: ", a, b)
}
