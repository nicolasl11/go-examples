package main

import "fmt"

func main() {
	// create a map
	var m1 map[string]string

	// initialize the map
	m1 = make(map[string]string, 1)

	// put something into the map
	m1["one"] = "een"
	m1["two"] = "twee"

	// check if it is in the map
	// if you only want to check if it is there -> replace 'val' with '_'
	if val, ok := m1["one"]; ok {
		// do something with val
		println("the value has been found: " + val)
	}

	// delete from the map

	// loop over the map
	for key, value := range m1 {
		fmt.Printf("key: %s, value: %s\n", key, value)
	}
}
