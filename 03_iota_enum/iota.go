package main

func main() {
	const (
		BLUE int = iota
		GREEN
		RED
		YELLOW
		ORANGE
	)

	println(BLUE)
}
