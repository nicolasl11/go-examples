package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
)

type Book struct {
	Title string
	Pages int
}

type Person struct {
	Name  string
	Age   int
	Books []Book
}

func (this Person) Bytes() []byte {
	var js, err = json.Marshal(this)

	if err != nil {
		panic(err.Error())
	}

	return js
}

func main() {
	var person = Person{
		Name: "hi",
		Age:  23,
		Books: []Book{
			Book{
				Title: "yoyo",
				Pages: 230,
			},
			Book{
				Title: "spok",
				Pages: 30,
			},
		},
	}

	var str = base64.StdEncoding.EncodeToString(person.Bytes())

	fmt.Printf("%s", str)
}
