package main

import (
	"encoding/xml"
	"fmt"
)

type Book struct {
	Title string
	Pages int
}

type Books struct {
	ListOfBooks []Book `xml:"book"`
}

type Person struct {
	Name  string `xml:"name"`
	Age   int    `xml:"age"`
	Books Books  `xml:"books"`
}

func main() {
	var person = Person{
		Name: "hi",
		Age:  23,
		Books: Books{
			ListOfBooks: []Book{
				Book{
					Title: "yoyo",
					Pages: 230,
				},
				Book{
					Title: "spok",
					Pages: 30,
				},
			},
		},
	}

	var data, err = xml.MarshalIndent(person, "  ", "    ")

	if err != nil {
		fmt.Printf(err.Error())
	}

	fmt.Printf("%s", data)
}
