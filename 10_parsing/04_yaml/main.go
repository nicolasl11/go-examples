package main

import (
	"fmt"

	//yaml "gopkg.in/yaml.v2"
	"github.com/ghodss/yaml"
)

type Book struct {
	Title string
	Pages int
}

type Person struct {
	Name  string
	Age   int
	Books []Book
}

func main() {
	var person = Person{
		Name: "hi",
		Age:  23,
		Books: []Book{
			Book{
				Title: "yoyo",
				Pages: 230,
			},
			Book{
				Title: "spok",
				Pages: 30,
			},
		},
	}

	var data, err = yaml.Marshal(person)

	if err != nil {
		fmt.Printf(err.Error())
	}

	fmt.Printf("%s", data)
}
