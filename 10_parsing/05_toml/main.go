package main

import (
	"bytes"
	"fmt"

	"github.com/BurntSushi/toml"
)

type Book struct {
	Title string
	Pages int
}

type Person struct {
	Name  string
	Age   int
	Books []Book
}

func main() {
	var person = Person{
		Name: "hi",
		Age:  23,
		Books: []Book{
			Book{
				Title: "yoyo",
				Pages: 230,
			},
			Book{
				Title: "spok",
				Pages: 30,
			},
		},
	}

	var data bytes.Buffer
	var err = toml.NewEncoder(&data).Encode(person)

	if err != nil {
		fmt.Printf(err.Error())
	}

	fmt.Printf("%s", data.Bytes())
}
