package main

import (
	"flag"
	"fmt"
)

var (
	number int
	text   string
	yes    bool
)

func init() {
	flag.IntVar(&number, "number", 12, "this is a test number")
	flag.StringVar(&text, "text", "yolo", "this is text")
	yes = *flag.Bool("bool", false, "no it true")
}

func main() {
	flag.Parse()
	fmt.Printf("number = %d\n", number)
	fmt.Printf("text = %s\n", text)
	fmt.Printf("yes = %b\n", yes)
}
