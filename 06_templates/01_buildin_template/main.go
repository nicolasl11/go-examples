package main

import "os"
import "text/template"

func main() {
	tmpl, err := template.ParseFiles("template.gohtml")

	if err == nil {
		tmpl.Execute(os.Stdout, "gorge")
	}
}
