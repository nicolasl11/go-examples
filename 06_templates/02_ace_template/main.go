package main

import "os"
import "github.com/yosssi/ace"

func main() {
	basic()
	inner()
}

func basic() {
	tmpl, err := ace.Load("template", "", nil)

	if err == nil {
		tmpl.Execute(os.Stdout, "gorge")
	}
}

func inner() {
	tmpl, err := ace.Load("basic", "inner", nil)

	if err == nil {
		tmpl.Execute(os.Stdout, "gorge")
	}
}
