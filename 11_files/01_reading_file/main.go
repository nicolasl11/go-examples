package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	readWithOS()
	readWithIO()
}

func readWithOS() {
	var fi, err = os.Open("input.txt")

	if err != nil {
		panic(err)
	}

	var b = make([]byte, 1000)

	fi.Read(b)

	fmt.Println(string(b))
}

func readWithIO() {
	var data, err = ioutil.ReadFile("input.txt")

	if err != nil {
		panic(err)
	}

	fmt.Println(string(data))
}
