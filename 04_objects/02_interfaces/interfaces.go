package main

import "fmt"

type Shape interface {
	Area() float64
	Perimeter() float64
}

type Rect struct {
	Width  float64
	Height float64
}

type Circle struct {
	Radius float64
}

func (r *Rect) Area() float64 {
	return r.Width * r.Height
}

func (r *Rect) Perimeter() float64 {
	return 2 * (r.Width + r.Height)
}

func (c *Circle) Area() float64 {
	return 3.14 * c.Radius * c.Radius
}

func (c *Circle) Perimeter() float64 {
	return 2 * 3.14 * c.Radius
}

func main() {
	rect := &Rect{
		Width:  4.0,
		Height: 3.0,
	}

	circle := &Circle{
		Radius: 3.0,
	}

	fmt.Println("Rect area: ", rect.Area())
	fmt.Println("Rect perimeter: ", rect.Perimeter())
	fmt.Println("Circle area: ", circle.Area())
	fmt.Println("Circle perimeter: ", circle.Perimeter())
}
