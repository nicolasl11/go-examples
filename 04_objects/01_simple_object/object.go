package main

import "strconv"

type Person struct {
	Name string
	Age  int
}

func (p *Person) SetName(name string) {
	p.Name = name
}

func (p *Person) SetAge(age int) {
	p.Age = age
}

func (p *Person) ToString() string {
	return "Person: [name = " + p.Name + ", age = " + strconv.Itoa(p.Age) + "]"
}
