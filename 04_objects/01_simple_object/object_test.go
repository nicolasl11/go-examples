package main

import "testing"

func TestGetName(t *testing.T) {
	var person *Person = &Person{
		Name: "john",
		Age:  30,
	}

	if person.Name != "john" {
		t.Error("expected", "john", "got", person.Name)
	}
}

func TestGetAge(t *testing.T) {
	var person *Person = &Person{
		Name: "john",
		Age:  30,
	}

	if person.Age != 30 {
		t.Error("expected", "30", "got", person.Age)
	}
}

func TestToString(t *testing.T) {
	var person *Person = &Person{
		Name: "john",
		Age:  30,
	}

	if person.ToString() != "Person: [name = john, age = 30]" {
		t.Error("expected", "Person: [name = john, age = 30]", "got", person.ToString())
	}
}
