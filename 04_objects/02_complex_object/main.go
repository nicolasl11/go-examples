package main

import "fmt"

type Person struct {
	Name string
	Age  int
}

type Teacher struct {
	Person
	Grade   int
	Lessons []string
}

func main() {
	var teacher = Teacher{
		Person: Person{
			Name: "berry",
			Age:  20,
		},
		Grade: 12,
		Lessons: []string{
			"math",
			"biology",
			"chemistry",
		},
	}

	fmt.Println(teacher)
}
