package main

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"github.com/yosssi/ace"
	"gopkg.in/gorp.v1"
	"net/http"
)

type Page struct {
	Names []Name
}

type Name struct {
	ID   int64  `db:id`
	Name string `db:name`
}

var db *sql.DB
var dbmap *gorp.DbMap

func initDB() {
	db, _ := sql.Open("sqlite3", "hello.db")

	dbmap = &gorp.DbMap{Db: db, Dialect: gorp.SqliteDialect{}}

	dbmap.AddTableWithName(Name{}, "names").SetKeys(true, "ID")
	dbmap.CreateTablesIfNotExists()
}

func main() {
	initDB()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		template, err := ace.Load("templates/index", "", nil)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		var p Page = Page{
			Names: []Name{},
		}

		if _, err := dbmap.Select(&p.Names, "select * from names;"); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		if err = template.Execute(w, p); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

	})

	fmt.Println(http.ListenAndServe(":8080", nil))
}
