package main

import "fmt"
import "net/http"
import "database/sql"
import _ "github.com/mattn/go-sqlite3"
import "github.com/yosssi/ace"

type Page struct {
	Names []Name
}

type Name struct {
	ID   int
	Name string
}

func main() {
	db, _ := sql.Open("sqlite3", "hello.db")

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		template, err := ace.Load("templates/index", "", nil)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		var p Page = Page{
			Names: []Name{},
		}

		if err := db.Ping(); err == nil {
			rows, _ := db.Query("select * from names;")

			for rows.Next() {
				var n Name
				rows.Scan(&n.ID, &n.Name)
				p.Names = append(p.Names, n)
			}
		}

		if err = template.Execute(w, p); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

	})

	fmt.Println(http.ListenAndServe(":8080", nil))
}
